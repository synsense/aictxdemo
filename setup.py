from distutils.core import setup

setup(
    name="aiCtxDemo",
    version="0.1dev",
    packages=["aiCtxDemo"],
    license="All rights reserved aiCTX AG",
    install_requires=["pyqtgraph"],
    long_description=open("README.md").read(),
)
