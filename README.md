# aictxdemo

This repository will hold the demo window template to be used across various demos developed in the company.



# Installation

`$ pip install -e . --user`

# Usage Instructions

Initialize an `AiCtxMainWindow` object and add your GUI/widget to its the `QVBoxLayout` instance `vertLayout` using the `addWidget` method.


## Example code

```
from PyQt5.QtWidgets import QApplication, QFrame
from aiCtxDemo import AiCtxMainWindow


# Initialize the application main window
app = QApplication([])
demoWindow = AiCtxMainWindow(strTitle='My AiCTX demo')

# Add your widgets to the main window
demoWindow.addWidgetToLayout(myQtWidget, strech=0, frameStyle = QFrame.NoFrame)

# Run the visualization
demoWindow.show()
app.exec_()
```
