from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPalette, QColor


def getDarkThemePalette() -> QPalette:
    """
    Convenience function to create a dark theme akin to the pyqtgraph theme
    :returns QPalette:
    """
    palette = QPalette()
    palette.setColor(QPalette.Background, Qt.black)
    palette.setColor(QPalette.Foreground, Qt.gray)
    return palette


def getLightThemePalette() -> QPalette:
    """
    Convenience function to create a dark theme akin to the pyqtgraph theme
    :returns QPalette:
    """
    palette = QPalette()
    palette.setColor(QPalette.Background, Qt.white)
    palette.setColor(QPalette.Foreground, QColor('#008dd7'))
    return palette
