# The main window for aictx demos


## The basic template of this demo should look as follows.

##############################################################
# Logo - Demo title                                          #
##############################################################
# Spec = value                      Spec = value             #
##############################################################
#                                                            #
#                                                            #
#                                                            #
#                      Data Box                              #
#                                                            #
##############################################################

from pathlib import Path
import PyQt5
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QFrame
from .utils import getDarkThemePalette, getLightThemePalette
from PyQt5.QtSvg import QSvgGenerator
import warnings
from .widgets import QHLine, TitleBar, StatBar, ImageBoxView
from PyQt5.QtCore import pyqtSignal

class AiCtxMainWindow(QWidget):
    """
    The main window that contains/displays all components of a demo GUI
    """
    reset = pyqtSignal()

    def __init__(
        self,
        strTitle: str = None,
        bInitData: bool = True,
        bStatBar: bool = False,
        defaultFrameStyle: QFrame.Shape = QFrame.NoFrame,
        dark: bool = True,
    ):
        """
        :param strTitle: str Title of this demo.
        :param bInitData: bool Flag to initialize data or run just the gui
        """
        self.strTitle = strTitle
        self.bInitData = bInitData
        QWidget.__init__(self)

        self.setPalette(getDarkThemePalette() if dark else getLightThemePalette())

        # Set the layout
        self.vertLayout = QVBoxLayout()
        self.setLayout(self.vertLayout)
        titleBar = TitleBar(strTitle=strTitle)
        self.vertLayout.addWidget(titleBar)
        if bStatBar:
            # Add a horizontal line
            self.vertLayout.addWidget(QHLine())

            # Add stats bar
            self.statBar = StatBar(["SynOps", "Power"], ["", "mW"])
            self.vertLayout.addWidget(self.statBar)
            # Add a horizontal line
            self.vertLayout.addWidget(QHLine())

        # Necessary attributes and params for screen shots
        self.gensvg = QSvgGenerator()
        self.gensvg.setTitle(strTitle)
        self.gensvg.setDescription("screenshot")
        self.screenShotCount = 0
        # self.defaultFrameStyle = QFrame.Box | QFrame.Raised
        self.defaultFrameStyle = defaultFrameStyle

        # Init plots
        self.initPlots()

        # Initialize data
        self.initData()

    def keyPressEvent(self, event):
        """
        Register user key presses to perform some actions
        """
        # Save screen shot as svg file
        if event.key() == PyQt5.QtCore.Qt.Key_P:
            self.takeScreenShot()
        if event.key() == PyQt5.QtCore.Qt.Key_R:
            self.resetPlot()
            # In order the signal that is emmited to get caught a connection must be built
            self.reset.emit()   

    @property
    def screenShotFileName(self) -> str:
        return (
            "screenshot_"
            + self.strTitle.replace(" ", "")
            + "_{0:03d}.svg".format(self.screenShotCount)
        )

    def takeScreenShot(self):
        """
        Take a screenshot of the current window
        """
        # Find a file name
        while Path(self.screenShotFileName).is_file():
            self.screenShotCount += 1
            if self.screenShotCount >= 1000:
                self.screenShotCount = 0
                warnings.warn(
                    "Too Many screen shots already on disk. Overwriting file *_000.svg"
                )
                break
        self.gensvg.setFileName(self.screenShotFileName)
        # Set svg properties
        self.gensvg.setSize(self.size())
        self.gensvg.setViewBox(self.rect())
        # Render svg
        self.render(self.gensvg)
        print(
            "Screenshot generated and saved successfully to {0}".format(
                self.screenShotFileName
            )
        )

    def addWidgetToLayout(
        self, widget: QWidget, strech: int = 0, frameStyle: QFrame.Shape = None
    ):
        """
        Convenience function to make a consistent addition of widgets to the main window
        """
        if frameStyle is None:
            style = self.defaultFrameStyle
        else:
            style = frameStyle
        try:
            widget.setFrameStyle(style)
        except AttributeError as e:
            warnings.warn(
                "This is not a QFrame object and so does not have setFrameStyle attribute."
            )
        self.vertLayout.addWidget(widget, stretch=strech)

    def updateDisplay(self):
        """
        Over ride this method for updating your gui components
        """
        pass

    def initData(self):
        """
        Initialize your data by overiding this method
        """
        pass

    def initPlots(self):
        """
        Build your GUI by overriding this method
        """
        pass

    def runOneStep(self):
        """
        Override this method with code to be executed by the timer
        """
        self.fetchData()
        self.processData()
        self.updateDisplay()

    def fetchData(self):
        """
        Fetch the next chunk of data
        Override this method accordingly
        """
        pass

    def processData(self):
        pass

    def resetPlot(self):
        pass