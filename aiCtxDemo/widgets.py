from pathlib import Path
import pyqtgraph as pg
import PyQt5
from PyQt5.QtWidgets import QLabel, QWidget, QVBoxLayout, QHBoxLayout, QFrame
from PyQt5.QtGui import QPixmap
from typing import List


class QHLine(QFrame):
    """
    Convenience class for a horizontal line
    """

    def __init__(self):
        QFrame.__init__(self)
        self.setFrameShape(QFrame.HLine)
        self.setFrameShadow(QFrame.Sunken)


class TitleBar(QWidget):
    """
    Title bar that goes on the top of all aiCTX demos
    """

    def __init__(self, strTitle: str = None):
        """
        :param strTitle: str Title of this demo.
        """
        self.strTitle = strTitle
        QWidget.__init__(self)
        self.titleViewBox = QHBoxLayout()
        self.setLayout(self.titleViewBox)

        # Add logo
        strFolderPath = str(Path(__file__).resolve().parent)
        self.logo = QLabel()
        self.logo.setAlignment(PyQt5.QtCore.Qt.AlignCenter)
        pixmap = QPixmap(strFolderPath + "/img/SynSense-02white.png")
        self.logo.setPixmap(pixmap.scaled(100, 100, PyQt5.QtCore.Qt.KeepAspectRatio))
        self.titleViewBox.addWidget(self.logo)

        # Add label of the demo
        font = PyQt5.QtGui.QFont("Helvetica", 20, weight=50)
        font.bold()
        self.label = QLabel(self.strTitle)
        self.label.setAlignment(PyQt5.QtCore.Qt.AlignCenter)
        self.label.setFont(font)
        self.titleViewBox.addWidget(self.label, 2)


class StatBar(QWidget):
    """
    Box to display various statistics
    """

    def __init__(self, vStrStats: List = [], vStrUnits: List = []):
        """
        :param vStrStats: List of parameter names to be displayed
        :param vStrUnits: List of units for each of the parameters
        """
        QWidget.__init__(self)
        try:
            assert len(vStrStats) == len(vStrUnits)
        except AssertionError as e:
            raise Exception(
                "The number of statistics to be displayed does not match with the number of units provided"
            )

        self.layout = QHBoxLayout()
        self.setLayout(self.layout)

        # Define the font of labels
        self.labelFont = PyQt5.QtGui.QFont("Helvetica", 15, weight=30)
        self.labelFont.bold()
        self.labels = {}
        self.values = {}
        self.units = {}

        # Add all labels to the widget
        for i in range(len(vStrStats)):
            self.addAStatToDisplay(vStrStats[i], vStrUnits[i])

    def getStringForLabel(self, strStat: str, strUnit: str, fVal: float = 0) -> str:
        return "{0} = {2} {1}".format(strStat, strUnit, fVal)

    def addAStatToDisplay(self, strStat: str, strUnit: str, fVal: float = 0):
        """
        Add a status bar to the display
        :param strStat: String label of the parameter
        :param strUnit: String unit of the parameter
        :param fVal: float value of the stat
        """
        label = QLabel(self.getStringForLabel(strStat, strUnit, fVal))
        label.setFont(self.labelFont)
        self.layout.addWidget(label)
        self.labels[strStat] = label
        self.values[strStat] = fVal
        self.units[strStat] = strUnit

    def updateStat(self, strStat: str, fVal: float):
        """
        Update statistics
        :param strStat: String label of the parameter
        :param strUnit: String unit of the parameter
        :param fVal: float value of the stat
        """
        self.values[strStat] = fVal
        self.labels[strStat].setText(
            self.getStringForLabel(strStat, self.units[strStat], fVal)
        )


class ImageBoxView(QFrame):
    def __init__(self, strTitle: str = None):
        """
        A box with non overlapping text and image
        """
        QWidget.__init__(self)
        layout = QVBoxLayout()
        self.setLayout(layout)
        # Add label
        self.label = QLabel(strTitle)
        self.label.setAlignment(PyQt5.QtCore.Qt.AlignCenter)
        layout.addWidget(self.label)
        # Add image
        self.imageView = pg.ImageView()
        self.imageView.ui.histogram.hide()
        self.imageView.ui.roiBtn.hide()
        self.imageView.ui.menuBtn.hide()
        layout.addWidget(self.imageView)
