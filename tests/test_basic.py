from aiCtxDemo import AiCtxMainWindow
from PyQt5.QtWidgets import QApplication, QFrame, QLabel


def test_basic_mainwindow():
    app = QApplication([])
    demowindow = AiCtxMainWindow(
        strTitle="Demo window",
        bStatBar=True,
        defaultFrameStyle=QFrame.Box | QFrame.Raised,
    )
    demowindow.show()
    app.exec_()


def test_window_with_widget_no_frame():
    app = QApplication([])
    demowindow = AiCtxMainWindow(
        strTitle="Demo window, with an added widget, No Frame",
        bStatBar=True,
        defaultFrameStyle=QFrame.NoFrame,
    )
    demowindow.addWidgetToLayout(QLabel("Some test widget"))
    demowindow.show()
    app.exec_()


def test_window_with_widget_frame():
    app = QApplication([])
    demowindow = AiCtxMainWindow(
        strTitle="Demo window, with an added widget, with Frame",
        bStatBar=True,
        defaultFrameStyle=QFrame.Box | QFrame.Raised,
    )
    demowindow.addWidgetToLayout(QLabel("Some test widget"))
    demowindow.show()
    app.exec_()
