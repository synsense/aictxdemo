from aiCtxDemo import AiCtxMainWindow
from aiCtxDemo.widgets import ImageBoxView, StatBar
from PyQt5.QtWidgets import QApplication, QFrame
import numpy as np
from PyQt5 import QtCore


class SpeckReadout(AiCtxMainWindow):
    def __init__(self, labels, images=None, buffer=None):
        super().__init__(
            strTitle="Speck Readout",
            bStatBar=False,
            defaultFrameStyle=QFrame.Box | QFrame.Raised,
        )
        self.labels = labels
        self.images = images
        self.counts = np.zeros(len(labels), dtype=int)
        self.buffer = buffer  # samna buffer to read from

    def initPlots(self):
        self.addWidgetToLayout(ImageBoxView())
        self.addWidgetToLayout(StatBar(["Label"], [""]))

    def updateDisplay(self):
        # Update image
        imgView: ImageBoxView = self.vertLayout.itemAt(1).widget()
        imgView.imageView.setImage(np.random.rand(30, 30))

        # Update readout
        label: StatBar = self.vertLayout.itemAt(2).widget()
        label.updateStat("Label", self.labels[self.counts.argmax()])

    def fetchData(self):
        # Reset counts
        self.counts[...] = 0
        #for ev in self.buffer.get_buff():
        #    self.counts[ev.feature] += 1


# TODO: Read from samna
def test_readout_gui():
    app = QApplication([])

    demowindow = SpeckReadout(labels=["Close", "Open"])
    demowindow.show()

    timer = QtCore.QTimer()
    timer.timeout.connect(demowindow.runOneStep)
    timer.start(1)

    app.exec_()
